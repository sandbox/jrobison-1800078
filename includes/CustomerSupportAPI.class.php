<?php

class CustomerSupportAPI {
  
  static private $_instance = null;
  
  private function __construct() {}
  
  static public function getInstance() {
    if(self::$_instance === null) {
      self::$_instance = new self();
    }
    return self::$_instance;
  }
  
  public function getCommands($csr) {
    $result = db_query('SELECT command FROM {customer_support_command} WHERE csr = :csr AND executed = 0', 
      array(':csr' => $csr)
    );
    
    $results = array();
    
    while(($row = db_fetch_array($result)) !== false) {
      $results[] = $row;
    }
    
    return $results;
  }
  
  public function generateCsr() {
    global $user;
    
    // We might not want this for the admin.
    //if($user->uid == 1) { return false; }
    
    // Grab the csr for the current user
    $hash = md5($_SERVER['HTTP_USER_AGENT'] . '::' . $_SERVER['REMOTE_ADDR']);
    $csr = (!empty($_SESSION['customer_support_csr'])) 
      ? $_SESSION['customer_support_csr'] 
      : $this->getCsr($user->uid, $hash);

    // Either read the csr from the db or grab from the current session
    if(empty($csr)) {
      // Build CSR and try not to squash some else's CSR
      while(true) {
        $csr = intval(substr(uniqid(rand(),true), 0, 9));
        if(!$this->csrExists($csr)) {
          // Must be unique to make it here
          $_SESSION['customer_support_csr'] = $csr;
          $this->insertCsr($csr, $user->uid, $hash);
          break;
        }
      }
    }
    
    return $csr;
  }
  
  public function getUidFromCsr($csr) {
    $result = db_query('SELECT uid FROM {customer_support_csr} WHERE csr = :csr', 
      array(':csr' => $csr)
    );
    $result = db_fetch_array($result);
    
    // Doesn't already exist then return true
    if(empty($result) || empty($result['uid'])) {
      return null;
    }
    return $result['uid'];
  }
  
  public function getUserFromCsr($csr) {
    $uid = $this->getUidFromCsr($csr);
    
    // Load the user
    if(!empty($uid)) {
      return user_load($uid);
    }
    
    return null;
  }
  
  public function sendCommandToUserByCsr($csr, $command) {
    // Send the command to the user
    if(!in_array($command, $user->data['commands'])) {
      if(!$this->csrCommandExists($csr, $command)) {
        $this->insertCsrCommand($csr, $command);
        return true;
      }
    }
    
    return false;
  }
  
  public function removeCsr($csr) {
    db_query('DELETE FROM {customer_support_csr} WHERE csr = :csr', 
      array(':csr' => $csr)
    );
    return true;
  }
  
  public function removeCsrByUid($uid) {
    db_query('DELETE FROM {customer_support_csr} WHERE uid = :uid', 
      array(':uid' => $uid)
    );
    return true;
  }
  
  public function setCsrCurrentUrl($csr, $currentUrl) {
    $previousUrl = (!empty($_SESSION['customer_support_current_url'])) ? $_SESSION['customer_support_current_url'] : null;
    if(empty($previousUrl) || $previousUrl != $currentUrl) {
      $_SESSION['customer_support_current_url'] = $currentUrl;
      db_query('UPDATE {customer_support_csr} SET current_url = :current_url WHERE csr = :csr', 
        array(':csr' => $csr, ':current_url' => $currentUrl)
      );
      return true;
    }
    return false;
  }
  
  public function executeCsrCommand($csr, $command) {
    db_query('UPDATE {customer_support_command} SET executed = 1 WHERE csr = :csr AND command = :command', 
      array(':csr' => $csr, ':command' => $command)
    );
    return true;
  }
  
  private function insertCsr($csr, $uid, $hash) {
    db_query('INSERT INTO {customer_support_csr} VALUES (:csr, :uid, :hash, \'\')', 
      array(':csr' => $csr, ':uid' => $uid, ':hash' => $hash)
    );
    return true;
  }
  
  private function insertCsrCommand($csr, $command) {
    db_query('INSERT INTO {customer_support_command} VALUES (:csr, :command, 0)', 
      array(':csr' => $csr, ':command' => $command)
    );
    return true;
  }
  
  /**
   * Grab the csr.  
   * 
   * Going directly to the DB might get slow eventually.  
   * We might need to cache this at some point.
   * 
   * @param type $uid
   * @param type $hash
   * @return null 
   */
  private function getCsr($uid, $hash) {
    $result = db_query('select csr from ( select @rownum:=@rownum+1 rownum, csr.* from (select @rownum:=0) r, customer_support_csr csr) t 
      WHERE uid = :uid AND hash = :hash order by rownum desc limit 1', 
      array(':uid' => $uid, ':hash' => $hash)
    );
    $result = db_fetch_array($result);
    
    if(!empty($result) && !empty($result['csr'])) {
      return intval($result['csr']);
    }
    return null;
  }
  
  private function csrExists($csr) {
    $result = db_query('SELECT count(*) as cnt FROM {customer_support_csr} 
      WHERE csr = :csr', 
      array(':csr' => $csr)
    );
    $result = db_fetch_array($result);
    
    if(!empty($result) && $result['cnt'] > 0) {
      return true;
    }
    return false;
  }
  
  private function csrCommandExists($csr, $command) {
    $result = db_query('SELECT count(*) as cnt FROM {customer_support_command} 
      WHERE csr = :csr AND command = :command AND executed = 0', 
      array(':csr' => $csr, ':command' => $command)
    );
    $result = db_fetch_array($result);
    
    if(!empty($result) && $result['cnt'] > 0) {
      return true;
    }
    return false;
  }
}