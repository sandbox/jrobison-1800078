<?php

/**
 * Implements hook_views_default_views().
 */
function customer_support_views_default_views() {
  $export = array();

  $view = new view;
  $view->name = 'customer_support_admin';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'users';
  $view->human_name = 'Customer Support Admin';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'access user profiles';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'name' => 'name',
    'csr' => 'csr',
    'current_url' => 'current_url',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'name' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'csr' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'current_url' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['override'] = 1;
  $handler->display->display_options['style_options']['sticky'] = 0;
  $handler->display->display_options['style_options']['empty_table'] = 0;
  /* Field: User: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'users';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['label'] = 'Username';
  $handler->display->display_options['fields']['name']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['name']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['name']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['name']['alter']['external'] = 0;
  $handler->display->display_options['fields']['name']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['name']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['name']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['name']['alter']['word_boundary'] = 0;
  $handler->display->display_options['fields']['name']['alter']['ellipsis'] = 0;
  $handler->display->display_options['fields']['name']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['name']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['name']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['name']['alter']['html'] = 0;
  $handler->display->display_options['fields']['name']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['name']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['name']['hide_empty'] = 0;
  $handler->display->display_options['fields']['name']['empty_zero'] = 0;
  $handler->display->display_options['fields']['name']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['name']['link_to_user'] = 1;
  $handler->display->display_options['fields']['name']['overwrite_anonymous'] = 0;
  $handler->display->display_options['fields']['name']['format_username'] = 1;
  /* Field: Customer Support: CSR Code */
  $handler->display->display_options['fields']['csr']['id'] = 'csr';
  $handler->display->display_options['fields']['csr']['table'] = 'customer_support_csr';
  $handler->display->display_options['fields']['csr']['field'] = 'csr';
  $handler->display->display_options['fields']['csr']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['csr']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['csr']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['csr']['alter']['external'] = 0;
  $handler->display->display_options['fields']['csr']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['csr']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['csr']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['csr']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['csr']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['csr']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['csr']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['csr']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['csr']['alter']['html'] = 0;
  $handler->display->display_options['fields']['csr']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['csr']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['csr']['hide_empty'] = 0;
  $handler->display->display_options['fields']['csr']['empty_zero'] = 0;
  $handler->display->display_options['fields']['csr']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['csr']['link_to_node'] = 0;
  /* Field: Customer Support: Current URL */
  $handler->display->display_options['fields']['current_url']['id'] = 'current_url';
  $handler->display->display_options['fields']['current_url']['table'] = 'customer_support_csr';
  $handler->display->display_options['fields']['current_url']['field'] = 'current_url';
  $handler->display->display_options['fields']['current_url']['alter']['alter_text'] = 1;
  $handler->display->display_options['fields']['current_url']['alter']['text'] = '<a href="[current_url]" target="_blank">[current_url]</a>';
  $handler->display->display_options['fields']['current_url']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['current_url']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['current_url']['alter']['external'] = 0;
  $handler->display->display_options['fields']['current_url']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['current_url']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['current_url']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['current_url']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['current_url']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['current_url']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['current_url']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['current_url']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['current_url']['alter']['html'] = 0;
  $handler->display->display_options['fields']['current_url']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['current_url']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['current_url']['hide_empty'] = 0;
  $handler->display->display_options['fields']['current_url']['empty_zero'] = 0;
  $handler->display->display_options['fields']['current_url']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['current_url']['link_to_node'] = 0;
  /* Field: Nice Redirect Button */
  $handler->display->display_options['fields']['php']['id'] = 'php';
  $handler->display->display_options['fields']['php']['table'] = 'views';
  $handler->display->display_options['fields']['php']['field'] = 'php';
  $handler->display->display_options['fields']['php']['ui_name'] = 'Nice Redirect Button';
  $handler->display->display_options['fields']['php']['label'] = 'Nice Redirect';
  $handler->display->display_options['fields']['php']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['php']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['php']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['php']['alter']['external'] = 0;
  $handler->display->display_options['fields']['php']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['php']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['php']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['php']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['php']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['php']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['php']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['php']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['php']['alter']['html'] = 0;
  $handler->display->display_options['fields']['php']['element_label_colon'] = 0;
  $handler->display->display_options['fields']['php']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['php']['hide_empty'] = 0;
  $handler->display->display_options['fields']['php']['empty_zero'] = 0;
  $handler->display->display_options['fields']['php']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['php']['use_php_setup'] = 0;
  $handler->display->display_options['fields']['php']['php_output'] = '<input type="text" class="csr-redirect" style="border: 1px solid black;" />
    <input type="submit" class="csr-redirect-submit" data-csr="<?php print $row->csr; ?>" value="Nice Redirect" />';
  $handler->display->display_options['fields']['php']['use_php_click_sortable'] = '0';
  $handler->display->display_options['fields']['php']['php_click_sortable'] = '';
  /* Field: Force Redirect Button */
  $handler->display->display_options['fields']['php_1']['id'] = 'php_1';
  $handler->display->display_options['fields']['php_1']['table'] = 'views';
  $handler->display->display_options['fields']['php_1']['field'] = 'php';
  $handler->display->display_options['fields']['php_1']['ui_name'] = 'Force Redirect Button';
  $handler->display->display_options['fields']['php_1']['label'] = 'Force Redirect';
  $handler->display->display_options['fields']['php_1']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['php_1']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['php_1']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['php_1']['alter']['external'] = 0;
  $handler->display->display_options['fields']['php_1']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['php_1']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['php_1']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['php_1']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['php_1']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['php_1']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['php_1']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['php_1']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['php_1']['alter']['html'] = 0;
  $handler->display->display_options['fields']['php_1']['element_label_colon'] = 0;
  $handler->display->display_options['fields']['php_1']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['php_1']['hide_empty'] = 0;
  $handler->display->display_options['fields']['php_1']['empty_zero'] = 0;
  $handler->display->display_options['fields']['php_1']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['php_1']['use_php_setup'] = 0;
  $handler->display->display_options['fields']['php_1']['php_output'] = '<input type="text" class="csr-force-redirect" style="border: 1px solid black;" />
    <input type="submit" class="csr-force-redirect-submit" data-csr="<?php print $row->csr; ?>" value="Force Redirect" />';
  $handler->display->display_options['fields']['php_1']['use_php_click_sortable'] = '0';
  $handler->display->display_options['fields']['php_1']['php_click_sortable'] = '';
  /* Field: Send Alert Button */
  $handler->display->display_options['fields']['php_2']['id'] = 'php_2';
  $handler->display->display_options['fields']['php_2']['table'] = 'views';
  $handler->display->display_options['fields']['php_2']['field'] = 'php';
  $handler->display->display_options['fields']['php_2']['ui_name'] = 'Send Alert Button';
  $handler->display->display_options['fields']['php_2']['label'] = 'Send Alert';
  $handler->display->display_options['fields']['php_2']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['php_2']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['php_2']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['php_2']['alter']['external'] = 0;
  $handler->display->display_options['fields']['php_2']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['php_2']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['php_2']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['php_2']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['php_2']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['php_2']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['php_2']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['php_2']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['php_2']['alter']['html'] = 0;
  $handler->display->display_options['fields']['php_2']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['php_2']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['php_2']['hide_empty'] = 0;
  $handler->display->display_options['fields']['php_2']['empty_zero'] = 0;
  $handler->display->display_options['fields']['php_2']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['php_2']['use_php_setup'] = 0;
  $handler->display->display_options['fields']['php_2']['php_output'] = '<input type="text" class="csr-alert" style="border: 1px solid black;" />
    <input type="submit" class="csr-alert-submit" data-csr="<?php print $row->csr; ?>" value="Send Alert" />';
  $handler->display->display_options['fields']['php_2']['use_php_click_sortable'] = '0';
  $handler->display->display_options['fields']['php_2']['php_click_sortable'] = '';
  /* Filter criterion: User: Active */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'users';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = '1';
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: User: Name */
  $handler->display->display_options['filters']['uid']['id'] = 'uid';
  $handler->display->display_options['filters']['uid']['table'] = 'users';
  $handler->display->display_options['filters']['uid']['field'] = 'uid';
  $handler->display->display_options['filters']['uid']['value'] = '';
  $handler->display->display_options['filters']['uid']['group'] = 1;
  $handler->display->display_options['filters']['uid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['uid']['expose']['operator_id'] = 'uid_op';
  $handler->display->display_options['filters']['uid']['expose']['label'] = 'Username';
  $handler->display->display_options['filters']['uid']['expose']['operator'] = 'uid_op';
  $handler->display->display_options['filters']['uid']['expose']['identifier'] = 'uid';
  $handler->display->display_options['filters']['uid']['expose']['multiple'] = FALSE;
  $handler->display->display_options['filters']['uid']['expose']['reduce'] = 0;
  /* Filter criterion: Customer Support: CSR Code */
  $handler->display->display_options['filters']['csr']['id'] = 'csr';
  $handler->display->display_options['filters']['csr']['table'] = 'customer_support_csr';
  $handler->display->display_options['filters']['csr']['field'] = 'csr';
  $handler->display->display_options['filters']['csr']['group'] = 1;
  $handler->display->display_options['filters']['csr']['exposed'] = TRUE;
  $handler->display->display_options['filters']['csr']['expose']['operator_id'] = 'csr_op';
  $handler->display->display_options['filters']['csr']['expose']['label'] = 'CSR Code';
  $handler->display->display_options['filters']['csr']['expose']['operator'] = 'csr_op';
  $handler->display->display_options['filters']['csr']['expose']['identifier'] = 'csr';
  $handler->display->display_options['filters']['csr']['expose']['multiple'] = FALSE;
  /* Filter criterion: User: Name */
  $handler->display->display_options['filters']['uid_1']['id'] = 'uid_1';
  $handler->display->display_options['filters']['uid_1']['table'] = 'users';
  $handler->display->display_options['filters']['uid_1']['field'] = 'uid';
  $handler->display->display_options['filters']['uid_1']['operator'] = 'not in';
  $handler->display->display_options['filters']['uid_1']['value'] = array(
    0 => '1',
  );
  $handler->display->display_options['filters']['uid_1']['expose']['operator_id'] = 'uid_1_op';
  $handler->display->display_options['filters']['uid_1']['expose']['label'] = 'Username NOT';
  $handler->display->display_options['filters']['uid_1']['expose']['operator'] = 'uid_1_op';
  $handler->display->display_options['filters']['uid_1']['expose']['identifier'] = 'uid_1';
  $handler->display->display_options['filters']['uid_1']['expose']['multiple'] = FALSE;
  $handler->display->display_options['filters']['uid_1']['expose']['reduce'] = 0;
  /* Filter criterion: Customer Support: CSR Code */
  $handler->display->display_options['filters']['csr_1']['id'] = 'csr_1';
  $handler->display->display_options['filters']['csr_1']['table'] = 'customer_support_csr';
  $handler->display->display_options['filters']['csr_1']['field'] = 'csr';
  $handler->display->display_options['filters']['csr_1']['operator'] = '>';
  $handler->display->display_options['filters']['csr_1']['value']['value'] = '0';
  $translatables['customer_support_admin'] = array(
    t('Master'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('Username'),
    t('CSR Code'),
    t('Current URL'),
    t('<a href="[current_url]" target="_blank">[current_url]</a>'),
    t('Nice Redirect'),
    t('Force Redirect'),
    t('Send Alert'),
    t('Username NOT'),
  );
  
  $export['customer_support_admin'] = $view;

  return $export;
}
