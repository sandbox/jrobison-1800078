(function($) {

  Drupal.behaviors.customer_support = {
    attach: function(context) {
      var support = new CustomerSupport();
      support.init();
    }
  };

  /**
  * Class used for holding customer support object
  */
  function CustomerSupport() {
    var csr, commands;

    // Generate or retrieve the csr
    var getCsr = function() {
      if(typeof(csr) == 'undefined') {
        csr = CustomerSupportGenerator.getCsr();
      }
      //console.log('CSR: ');
      //console.log(csr);
      return csr;
    }

    // Poll commands for csr
    var executeCommands = function() {
      commands = CustomerSupportPoller.executeCommands(getCsr());
      //console.log('COMMANDS: ');
      //console.log(commands);
      return commands;
    }

    // Initializing the poller and click functions...
    CustomerSupport.prototype.init = function() {
      getCsr();
      
      // Turn on polling
      setInterval(
        function() {
          executeCommands();
        },
        1500
      );
        
      // Turn on url updating
      setInterval(
        function() {
          CustomerSupportCommandSender.updateCurrentUrl(csr);
        },
        1500
      );
        
      // Attach click to redirect submit
      $('.csr-redirect-submit').click(function() {
        input = $(this).siblings('input')[0];
        if(typeof(input) != 'undefined' && $(input).val() != ''
          && typeof($(this).data('csr')) != 'undefined' && $(this).data('csr') != '') 
        {
          command = 'redirect|' + $(input).val();
          CustomerSupportCommandSender.sendCommand($(this).data('csr'), command);
        }
      });
      
      // Attach click to force redirect submit
      $('.csr-force-redirect-submit').click(function() {
        input = $(this).siblings('input')[0];
        if(typeof(input) != 'undefined' && $(input).val() != ''
          && typeof($(this).data('csr')) != 'undefined' && $(this).data('csr') != '') 
        {
          command = 'force-redirect|' + $(input).val();
          CustomerSupportCommandSender.sendCommand($(this).data('csr'), command);
        }
      });
      
      // Attach click to alert submit
      $('.csr-alert-submit').click(function() {
        input = $(this).siblings('input')[0];
        if(typeof(input) != 'undefined' && $(input).val() != ''
          && typeof($(this).data('csr')) != 'undefined' && $(this).data('csr') != '') 
        {
          command = 'alert|' + $(input).val();
          CustomerSupportCommandSender.sendCommand($(this).data('csr'), command);
        }
      });
    }
  }

/****************************** CSR GENERATOR   **********************************/


  /**
  * Class used for generating the CSR #
  */
  function CustomerSupportGenerator() {
  }

  CustomerSupportGenerator.getCsr = function() {
    var response = null;

    $.ajax({
      url: '/customer_support/api/generateCsr',
      async: false,
      dataType: 'json',
      data: {'csr_js': true},
      success: function(data) {
        response = data;
      }
    });

    return response;
  };
  
  /****************************** COMMAND POLLER   **********************************/

  /**
  * Class used for polling customer support commands
  */
  function CustomerSupportPoller() {
  }

  CustomerSupportPoller.executeCommands = function(csr) {
    $.ajax({
      url: '/customer_support/api/getCommands',
      async: true,
      dataType: 'json',
      data: {'csr': csr, 'csr_js': true},
      success: function(data) {
        if(typeof(data) != 'undefined' && data != null) {
          CustomerSupportCommandInterpreter.execute(csr, data);
          return true;
        }
      }
    });

    return true;
  };
  

 /****************************** COMMAND INTERPRETER   **********************************/

 /**
  * Class used for executing customer support commands
  */
  function CustomerSupportCommandInterpreter() {
  }


  CustomerSupportCommandInterpreter.execute = function(csr, commands) {
    if(typeof(commands) == 'object' && commands.length > 0) {
      for(i in commands) {
        command = commands[i].command;
        parts = command.split('|');
        
        // Set it as executed right away
        $.ajax({
          url: '/customer_support/api/executeCsrCommand',
          async: true,
          dataType: 'json',
          data: {'csr': csr, 'command': command, 'csr_js': true},
          success: function(data) {
            return true;
          }
        });
        
        // What kind of command is this?
        switch(parts[0]) {
          case 'alert':
            alert(parts[1]);
            break;
          case 'redirect':
            if(parts[1].indexOf('/') == 0) {
              parts[1] = parts[1].substring(1, parts[1].length);
            }
            $.ajax({
              url: location.protocol + '//' + location.host + '/' + parts[1], 
              data: {'csr_js': true},
              success: function(data, textStatus) {
                // URL is good, redirect
                if(location.href != location.protocol + '//' + location.host + '/' + parts[1]) {
                  confirmed = confirm(
                    'A customer service representative is redirecting you to: ' + location.protocol + '//' + location.host + '/' + parts[1]
                    + "\nPress OK to continue."
                  );

                  // Only redirect if the user says OK
                  if(confirmed) {
                    location.href = location.protocol + '//' + location.host + '/' + parts[1];
                  }
                }
              },
              error: function(jqXHR, textStatus, errorThrown) {
                // URL is bad - Do nothing
              }
            });
            break;
          case 'force-redirect':
            if(parts[1].indexOf('/') == 0) {
              parts[1] = parts[1].substring(1, parts[1].length);
            }
            $.ajax({
              url: location.protocol + '//' + location.host + '/' + parts[1], 
              data: {'csr_js': true},
              success: function(data, textStatus) {
                // URL is good, redirect
                if(location.href != location.protocol + '//' + location.host + '/' + parts[1]) {
                  location.href = location.protocol + '//' + location.host + '/' + parts[1];
                }
              },
              error: function(jqXHR, textStatus, errorThrown) {
                // URL is bad - Do nothing
              }
            });
            break;
          default:
            // Do nothing
            break;
        }
        
        
      }
    }
  };
  
  
  /****************************** COMMAND SENDER   **********************************/
  
  /**
  * Class used for executing customer support commands
  */
  function CustomerSupportCommandSender() {
  }

  CustomerSupportCommandSender.sendCommand = function(csr, command) {
    $.ajax({
      url: '/customer_support/api/sendCommandToUserByCsr',
      async: true,
      dataType: 'json',
      data: {'csr': csr, 'command': command, 'csr_js': true},
      success: function(data) {
        return true;
      }
    });
  };
  
  CustomerSupportCommandSender.updateCurrentUrl = function(csr) {
    $.ajax({
      url: '/customer_support/api/setCsrCurrentUrl',
      async: true,
      dataType: 'json',
      data: {'csr': csr, 'current_url': location.href, 'csr_js': true},
      success: function(data) {
        return true;
      }
    });
  };
  
})(jQuery);